from __future__ import annotations

import asyncio
import json
from typing import Any
from pathlib import Path

class Command():

    def __init__(
        self,
        cmd: str,
        cwd: str = None,
        successful: bool = False
    ):
        self.cmd = cmd
        self.cwd = cwd
        self.successful = successful

    def __str__(self) -> str:
        return (
            f"{{ "
            f"cmd: {self.cmd}, "
            f"directory: {self.cwd}, "
            f"successful: {self.successful} "
            f"}}"
        )

    def __eq__(self, other: Any) -> bool:
        if other is None:
            return False

        elif not isinstance(other, Command):
            return False

        elif not (hasattr(other, 'cmd')
            and self.cmd == other.cmd):
            return False
        elif not (hasattr(other, 'cwd')
            and self.cwd == other.cwd):
            return False
        else:
            return True

    @classmethod
    def from_dict(cls, dict_cmd: dict) -> Command:
        return cls(
            cmd=dict_cmd.get('cmd', None),
            cwd=dict_cmd.get('cwd', None),
        )

    @classmethod
    def from_json(cls, json_cmd: str) -> Command:
        dictionary = json.loads(json_cmd)
        return cls.from_dict(dictionary)

    def to_json(self) -> str:
        return json.dumps(self, default=lambda ob: ob.__dict__)

    async def run(self) -> dict:

        def parse_path(path: str | None) -> Path | None:
            if not path:
                return None

            if '$HOME' in path:
                path_list = path.split('/')
                return Path.home().joinpath(*path_list[1:])

            return Path(path)

        ret = {}
        try:
            proc = await asyncio.create_subprocess_shell(
                self.cmd,
                cwd=parse_path(self.cwd),
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE
            )

            stdout, stderr = await proc.communicate()

            ret['stdout'] = stdout.decode('utf-8').replace("'", "").splitlines()
            ret['stderr'] = stderr.decode('utf-8').replace("'", "").splitlines()
            ret['returncode'] = proc.returncode

            if proc.returncode == 0:
                self.successful = True

        except FileNotFoundError as e:
            ret['stderr'] = [f"FileNotFoundError: {e}"]
            ret['returncode'] = 1


        return ret

