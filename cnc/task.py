from __future__ import annotations

import json
import logging
from datetime import datetime
from enum import Enum
from typing import Any, List, Dict

from cnc.command import Command
from cnc.errors import CnCUnknownTaskState
from cnc.utils.json import default

class State(Enum):
    COMPLETED = 1
    RUNNING   = 2
    WAITING   = 3
    DEPENDANT = 4
    CANCELED  = 5

    @classmethod
    def from_dict(cls, state: str) -> State:
        if state == 'COMPLETED':
            return cls.COMPLETED
        if state == 'RUNNING':
            return cls.RUNNING
        if state == 'WAITING':
            return cls.WAITING
        if state == 'DEPENDANT':
            return cls.DEPENDANT
        if state == 'CANCELED':
            return cls.CANCELED

        raise CnCUnknownTaskState

class DependsOn():

    def __init__(self, task_name: str, agent: str = None):
        self.task_name = task_name
        self.agent = agent

    @classmethod
    def from_dict(cls, depends_on: dict) -> DependsOn:
        return cls(
            task_name=depends_on.get('task_name', None),
            agent=depends_on.get('agent', None)
        )

class Task():

    def __init__(
            self,
            name: str,
            command: Command,
            state: State = State.WAITING,
            time_started: datetime = None,
            time_completed: datetime = None,
            result: dict = None,
            concurrent: bool = True,
            depends_on: List[DependsOn] = [],
        ):
        self.name = name
        self.command = command
        self.state = state
        self.time_started = time_started
        self.time_completed = time_completed
        self.result = result
        self.concurrent = concurrent
        self.depends_on = depends_on

    def __str__(self):
        return (
            f"{self.name}: {{ command: {self.command} "
            f"state: {self.state} "
            f"result: {self.result} "
            f"concurrent: {self.concurrent} "
            f"depends_on: {self.depends_on} }}"
        )

    def __eq__(self, other: Any) -> bool:
        if other is None:
            return False

        elif not isinstance(other, Task):
            return False

        elif not (hasattr(other, 'name')
            and self.name == other.name):
            return False
        elif not (hasattr(other, 'command')
            and self.command == other.command):
            return False
        elif not (hasattr(other, 'concurrent')
            and self.concurrent == other.concurrent):
            return False
        # elif not (hasattr(other, 'depends_on')
        #     and self.depends_on == other.depends_on):
        #     return False
        else:
            return True

    @classmethod
    def from_dict(cls, dict_task: Dict[Any, Any]) -> Task:

        command = Command.from_dict(dict_task.get('command', None))
        state = State.from_dict(dict_task.get('state', None))
        depends_on = [
            DependsOn.from_dict(do)
            for do in dict_task.get('depends_on', [])
        ]

        return cls(
            name=dict_task['name'],
            command=command,
            state=state,
            time_started=dict_task.get('time_started', None),
            time_completed=dict_task.get('time_completed', None),
            result=dict_task.get('result', None),
            concurrent=dict_task.get('concurrent', True),
            depends_on=depends_on,
        )

    @classmethod
    def from_json(cls, json_task: str) -> Task:
        dictionary = json.loads(json_task)
        return cls.from_dict(dictionary)

    def to_json(self) -> str:
        return json.dumps(self, default=default)

    async def run(self, verbosity) -> Task:
        self.state = State.RUNNING
        self.time_started = datetime.now().timestamp()
        self.result = await self.command.run()
        self.time_completed = datetime.now().timestamp()
        self.state = State.COMPLETED

        message = (
            f"Task '{self.name}' "
            f"{'Completed successfully' if self.command.successful else 'Failed'}"
        )
        # TODO: level one should no just add state, we should think about how
        # these should change based on 'v'
        if verbosity >= 1:
            message = message + f" state: {self.state}"
        if verbosity >= 2:
            message = message + f" result: {self.result}"
        if verbosity == 3:
            message = self.to_json()

        if self.command.successful:
            logging.info(message)
        else:
            logging.warning(message)

        return self


