import re

from cnc.task import Task, State as TaskState
from cnc.command import Command

class TestTask:
    test_task = Task(
        name='test_command',
        command=Command(
            cmd='echo hello',
            cwd='.',
        ),
    )

    test_task_json = (
        '{'
            '"name": "test_command", '
            '"command": {'
                '"cmd": "echo hello", '
                '"cwd": ".", '
                '"successful": false'
            '}, '
            '"state": "WAITING", '
            '"time_started": null, '
            '"time_completed": null, '
            '"result": null, '
            '"concurrent": true, '
            '"depends_on": []'
        '}'
    )

    def test_default_init(self):
        assert self.test_task.name == 'test_command'
        assert self.test_task.state == TaskState.WAITING
        assert self.test_task.time_started == None
        assert self.test_task.time_completed == None
        assert self.test_task.result == None
        assert self.test_task.concurrent == True
        assert self.test_task.depends_on == []

    def test_from_dict(self):
        command = {
            'cmd': 'echo hello',
            'cwd': '.'
        }
        dict_task = {
            'name': 'test_command',
            'command': command,
            'state': 'WAITING',
        }

        from_dict_task = Task.from_dict(dict_task)

        assert from_dict_task == self.test_task

    def test_from_json(self):

        assert Task.from_json(self.test_task_json) == self.test_task

    def test_to_json(self):

        assert self.test_task.to_json() == self.test_task_json


