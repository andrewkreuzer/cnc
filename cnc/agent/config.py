from __future__ import annotations

from pathlib import Path
import yaml
import logging
from typing import List, Tuple, Dict

from cnc.config import BaseConfig

class Config(BaseConfig):

    def __init__(
        self,
        host: str = None,
        port: int = None,
        verbosity: int = None,
    ) -> None:
        self.host = host
        self.port = port
        self.verbosity = verbosity

    @classmethod
    def load(
        cls,
        config_path_str: str = None,
        cli_options: dict = {}
    ) -> Config:

        config_path = Config.get_path(config_path_str)

        with open(config_path, 'r') as f:
            config_file = yaml.load(f, Loader=yaml.FullLoader)
            agent_config = config_file.get('agents', None)

            if not agent_config:
                logging.warn("No agent configuration found in config file")
                agent_config = {}

            return cls(
                host=(
                    cli_options.get('host', None)
                    or agent_config.get('host', None)
                    or 'localhost'
                ),
                port=(
                    cli_options.get('port', None)
                    or agent_config.get('port', None)
                    or 8765
                ),
                verbosity=(
                    cli_options.get('verbosity', None)
                    or agent_config.get('verbosity', None)
                    or 0
                ),
            )

