#!/usr/bin/env python

import site
import pathlib
import sys
from setuptools import find_packages, setup
from version import __version__

setup(
    name='cnc',
    version=__version__,
    description='An async command and control implmentation',
    author='Andrew Kreuzer',
    author_email='akreuzer.08@gmail.com',
    packages=find_packages(),
    entry_points={
        'console_scripts': [ 'cnc=cnc:cli']
    },
    install_requires=[
        'websockets',
        'pyyaml',
        'click',
        'python-daemon',
        'fasteners',
    ],
    data_files=[ ('cnc', ['config/config.yml'])],
)

