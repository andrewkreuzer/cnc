import websockets
from typing import Any, Dict, List

from ..task import Task

class MessageBroker():

    def __init__(self, websocket: websockets.WebSocketServerProtocol) -> None:
        self.websocket = websocket

    async def send(self, json_message: str):
        await self.websocket.send(json_message)

class Agent():

    def __init__(
        self,
        name: str,
        tasks: List[Task],
        subscriptions: List[str],
        message_broker: MessageBroker = None,
        ntasks: int = 0,
        ctasks: int = 0,
    ) -> None:

        self.name = name
        self.message_broker = message_broker
        self.tasks = tasks
        self.subscriptions = subscriptions
        self.ntasks = len(tasks)
        self.ctasks = 0

    def __str__(self) -> str:
        return (
            f"{self.name} has completed {self.ctasks}/{self.ntasks} tasks"
        )

    def set_message_broker(self, message_broker: MessageBroker):
        self.message_broker = message_broker
