from __future__ import annotations

from pathlib import Path
import yaml
import logging
from typing import List, Tuple, Dict

from .agent import Agent
from cnc.config import BaseConfig
from cnc.task import Task, DependsOn
from cnc.command import Command
from cnc.task import State as TaskState
from cnc.errors import (
    CnCAgentNotFoundError,
    CnCConfigNotFoundError,
    CnCConfigReadError
)

class Config(BaseConfig):

    def __init__(
        self,
        config_file: str,
        config_path: Path,
        host: str,
        port: int,
        verbosity: int,
        agents: Dict[str, Agent]
    ) -> None:
        self.config_path = config_path
        self.config_file = config_file
        self.host = host
        self.port = port
        self.verbosity = verbosity
        self.debug = False
        self.agents = agents
        self.completed_cache = 30

    @classmethod
    def load(
        cls,
        debug: bool = False,
        config_path_str: str = None,
        cli_options: dict = {}
    ) -> Config:

        config_path = Config.get_path(config_path_str)

        def _parse_depends_on(
            deps_list: List[str]
        ) -> Tuple[List[DependsOn], List[str]]:

            dependant_list: List[DependsOn] = []
            agent_subscriptions: List[str] = []

            for dep in deps_list:
                s = dep.split(':')

                if len(s) == 1:
                    dependant_list.append(DependsOn(task_name=s[0]))
                elif len(s) == 2:
                    dependant_list.append(DependsOn(task_name=s[1], agent=s[0]))
                    agent_subscriptions.append(s[0])
                else:
                    raise CnCConfigReadError

            return dependant_list, agent_subscriptions

        def _parse_task(task: dict) -> Tuple[Task, List[str]]:
            task_name = list(task.keys())[0]
            options = list(task.values())[0]

            depends_on, agent_subscriptions = (
                _parse_depends_on(options.get('depends_on', []))
            )

            if depends_on:
                state = TaskState.DEPENDANT
            else:
                state = TaskState.WAITING

            return (
                Task(
                    name=task_name,
                    state = state,
                    command=Command(
                        cmd=options.get('cmd', None),
                        cwd=options.get('cwd', '.')
                    ),
                    depends_on=depends_on,
                    concurrent=options.get('concurrent', True),
                ),

                agent_subscriptions
            )

        with open(config_path, 'r') as f:
            config_file = yaml.load(f, Loader=yaml.FullLoader)
            agent_list = config_file['tasks']
            agents: Dict[str, Agent] = {}
            for agent_dict in agent_list:
                for an, tl in agent_dict.items():
                    tasks = []
                    subscriptions = []
                    for task in tl:
                        t, agent_subs = _parse_task(task)
                        tasks.append(t)
                        subscriptions.extend(agent_subs)

                    agents[an] = Agent(
                            name=an,
                            tasks=tasks,
                            subscriptions=subscriptions
                    )

            logging.info(f"Config loaded")

            agent_config = config_file.get('server', None)
            if not agent_config:
                logging.warn("No agent configuration found in config file")
                agent_config = {}

            return cls(
                config_file=config_file,
                agents=agents,
                config_path=config_path,
                host=(
                    cli_options.get('host', None)
                    or agent_config.get('host', None)
                    or 'localhost'
                ),
                port=(
                    cli_options.get('port', None)
                    or agent_config.get('port', None)
                    or 8765
                ),
                verbosity=(
                    cli_options.get('verbosity', None)
                    or agent_config.get('verbosity', None)
                    or 0
                ),
            )

    def get_agent(self, agent_name: str) -> Agent:
        try:
            return self.agents[agent_name]
        except KeyError as e:
            logging.error(f"No agent found with the name {e}")
            raise CnCAgentNotFoundError(f"No agent found with the name {e}")

if __name__ == '__main__':
    Config.load(True)
