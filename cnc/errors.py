class CnCAgentNotFoundError(Exception):
    pass

class CnCConfigReadError(Exception):
    pass

class CnCConfigNotFoundError(Exception):
    pass

class CnCMessageBrokerNotFoundError(Exception):
    pass

class CnCUnknownTaskState(Exception):
    pass

class CnCDaemonizeError(Exception):
    pass
