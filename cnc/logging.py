import sys
import logging

def initialize_logging(debug: bool = False):

    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    if debug:
        handler.setLevel(logging.DEBUG)

    else:
        handler.setLevel(logging.INFO)

    formatter = logging.Formatter('%(levelname)s:[%(asctime)s] %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


