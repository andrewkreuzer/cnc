import websockets
import json
import logging

from cnc.messaging import Message

class MessageBroker():

    def __init__(self, websocket: websockets.WebSocketClientProtocol, agent_name: str):
        self.agent_name = agent_name
        self.websocket = websocket

    async def send(self, json_message: dict):
        tasks = json_message.get('tasks', None)

        if tasks:
            message = Message(
                tasks={ self.agent_name: tasks },
            ).to_json()

            await self.websocket.send(message)

    async def send_registration(self, agent_name: str):
        registration_message = json.dumps({ 'agent_name': agent_name })
        await self.websocket.send(registration_message)
        logging.info(f"Sending registration: {registration_message}")
