import asyncio
import logging
import sys
import time
import websockets
import daemon
import fasteners
import site
import sys
from pathlib import Path

from .config import Config
from .message_broker import MessageBroker
from .scheduler import Scheduler
from cnc.messaging import Message
from cnc.logging import initialize_logging
from cnc.errors import (
    CnCAgentNotFoundError,
    CnCDaemonizeError
)

class Agent():

    def __init__(self,
        agent_name: str = None,
        debug: bool = False,
        config_path_str: str = None,
        cli_options: dict = None,
    ):
        if not agent_name:
            raise CnCAgentNotFoundError

        initialize_logging(debug=debug)

        self.agent_name = agent_name
        self.config = Config().load(config_path_str, cli_options)
        self.scheduler = Scheduler(self.config)
        self.message_broker = None

        self.connection_sleep = 5
        self.connection_attempts = 3

        self.uri = f"ws://{self.config.host}:{self.config.port}"

    def run(self, daemonize) -> None:
        if daemonize:
            # User vs system install, if cnc was installed with root the
            # working_directory would be /usr/local/lib/cnc if installed as a user
            # the working_directory is ~/.local/lib/cnc
            if Path(site.USER_BASE + '/cnc').is_dir():
                working_directory = site.USER_BASE + '/cnc'
                pidfile = fasteners.InterProcessLock('/tmp/cnc.pid')
                stdout = open(site.USER_BASE + '/cnc/cnc.log', 'a')
                stderr = open(site.USER_BASE + '/cnc/cnc.error', 'a')
            elif Path(sys.prefix + '/cnc').is_dir():
                working_directory = sys.prefix + '/cnc'
                pidfile = fasteners.InterProcessLock('/var/run/cnc.pid')
                stdout = open(sys.prefix + '/cnc/cnc.log', 'a')
                stderr = open(sys.prefix + '/cnc/cnc.error', 'a')
            else:
                raise CnCDaemonizeError

            context = daemon.DaemonContext(
                detach_process=True,
                pidfile=pidfile,
                working_directory=working_directory,
                stdout=stdout,
                stderr=stderr,
            )

            with context:
                asyncio.get_event_loop().run_until_complete(self.handle())

        else:
            asyncio.get_event_loop().run_until_complete(self.handle())

    async def handle(self) -> None:
        asyncio.create_task(self.scheduler.run())
        self.scheduler.run_state('RUNNING')

        attempts = 0
        while True:
            try:
                async with websockets.connect(self.uri) as websocket:
                    attempts = 0
                    self.message_broker = MessageBroker(agent_name=self.agent_name, websocket=websocket)
                    self.scheduler.set_message_broker(self.message_broker)

                    await self.message_broker.send_registration(self.agent_name)

                    async for json_message in websocket:
                        message = Message.from_json(json_message)
                        self.scheduler.update_from_message(message)

            except ConnectionRefusedError:
                logging.warning(f"Unable to connect, retrying connection in {self.connection_sleep} seconds")
                time.sleep(self.connection_sleep)
            except websockets.exceptions.ConnectionClosedError:
                logging.warning(f"Connection Closed")
                sys.exit()
            finally:
                attempts = attempts + 1
                if attempts >= self.connection_attempts:
                    logging.info(f"attempted {self.connection_attempts} connections, exitting")
                    sys.exit(1)


if __name__ == '__main__':
    Agent(debug=True, agent_name='client')
