import asyncio
from typing import Any, Dict, List
import websockets
import json
import logging
import sys

from websockets.server import WebSocketServerProtocol

from cnc.messaging import Message
from .config import Config
from .agent import *
from cnc.errors import CnCAgentNotFoundError, CnCConfigNotFoundError
from cnc.logging import initialize_logging
from cnc.task import Task, State as TaskState

class State():
    state = None

    def __init__(
        self,
        debug: bool = False,
        config_path: str = None,
        cli_options: dict = {}
    ) -> None:
        try:
            self.config = Config.load(
                debug=debug,
                config_path_str=config_path,
                cli_options=cli_options
            )
        except CnCConfigNotFoundError:
            logging.error("Config file not found")
            sys.exit(1)

        self.agents: Dict[str, Agent] = {}

    def __str__(self) -> str:
        return str(self.agents)

    def add_agent(self, agent_info: dict) -> Agent:
        agent_name = agent_info['name']
        agent = self.config.get_agent(agent_name)

        message_broker = MessageBroker(agent_info['websocket'])
        agent.set_message_broker(message_broker)
        self.agents[agent_name] = agent

        return agent

    def remove_agent(self, agent_name: str):
        del self.agents[agent_name]

    async def update(self, message: Message):
        if message.error:
            logging.error(message.error)
            return

        for agent_name, tasks in message.tasks.items():
            agent = self.agents[agent_name]

            completed_task_list: List[Task] = []
            for i, task in enumerate(agent.tasks):
                for mt in tasks:
                    if (
                        task == mt
                        and
                        task.state != mt.state
                    ):
                        agent.tasks[i] = mt
                        if mt.state == TaskState.COMPLETED:
                            completed_task_list.append(mt)

            agent.ctasks += len(completed_task_list)

            if completed_task_list:
                update_message = Message(
                    tasks={ agent_name: completed_task_list },
                ).to_json()
                for ainfo in self.agents.values():
                    if agent_name in ainfo.subscriptions:
                        await ainfo.message_broker.send(json_message=update_message)

                    logging.info(f"State: {ainfo}")

    def set_run_state(self, change_run_state: str):
        self.state = change_run_state

class Server():

    def __init__(
        self,
        debug: bool = False,
        config_path: str = None,
        cli_options: dict = {},
    ) -> None:

        initialize_logging(debug=debug)
        self.state = State(debug, config_path, cli_options)


    def run(self):
        logging.info(f"Starting Server")
        socket_server = websockets.serve(
            self.socket_handler,
            self.state.config.host,
            self.state.config.port
        )
        asyncio.get_event_loop().run_until_complete(socket_server)
        asyncio.get_event_loop().run_forever()

    async def socket_handler(self, websocket: WebSocketServerProtocol, _):
        self.state.set_run_state('RUNNING')
        self.message_broker = MessageBroker(websocket)

        await self.register(websocket)
        try:
            async for message in websocket:
                message = Message.from_json(message)
                await self.state.update(message)
        except websockets.exceptions.ConnectionClosedError:
            logging.info(f"Connection Closed")
        finally:
            await self.unregister(websocket)

    async def register(self, websocket: WebSocketServerProtocol):
        register_message = await websocket.recv()
        logging.info(f"Registering an agent: {register_message}")
        register_message = json.loads(register_message)

        agent_name = register_message['agent_name']
        message = None
        try:
            agent = self.state.add_agent({'name': agent_name, 'websocket': websocket})

            task_dict = {}
            for agent_name, ainfo in self.state.agents.items():
                if agent_name in agent.subscriptions:
                    task_dict[agent_name] = [ task for task in ainfo.tasks if task.state == TaskState.COMPLETED ]

            task_dict[agent.name] = agent.tasks

            message = Message(
                tasks = task_dict,
            )

        except CnCAgentNotFoundError:
            error_message = json.dumps({'Error': f"No agent with the name {agent_name}"})
            await websocket.send(error_message)
            await websocket.close()

        if message:
            await websocket.send(message.to_json())

    async def unregister(self, websocket) -> bool:
        for agent, agent_info in self.state.agents.items():
            if agent_info.message_broker.websocket == websocket:
                self.state.remove_agent(agent)
                logging.info(f"Unregistering connection for: {agent}")
                return True

        return False

if __name__ == '__main__':
    Server(debug=True)
