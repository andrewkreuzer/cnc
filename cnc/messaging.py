from __future__ import annotations

import json
from typing import Dict, List
import uuid

from .task import Task
from .utils.json import default

class Message():

    def __init__(
            self,
            tasks: Dict[str, List[Task]] = {},
            error: str = None,
    ):
        self.tasks = tasks
        self.error=error

    def __str__(self) -> str:
        tasks = [
            (aname, task.__dict__)
            for aname, tasks
            in self.tasks.items()
            for task in tasks
        ]
        return f"{tasks}"

    @classmethod
    def from_json(cls, message) -> Message:
        dictionary = json.loads(message)

        error = dictionary.get('Error', None)

        tasks = {
            an: [ Task.from_dict(t) for t in tl ]
            for an, tl in dictionary.get('tasks', {}).items()
        }

        return cls(
            tasks=tasks,
            error=error,
        )

    def to_json(self) -> str:
        return json.dumps(self, default=default)
