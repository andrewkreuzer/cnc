from enum import Enum

def default(obj):
    if isinstance(obj, Enum):
        return obj.name
    return obj.__dict__
