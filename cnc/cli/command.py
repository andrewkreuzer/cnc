import click
import pkg_resources

from cnc.server import Server
from cnc.agent import Agent

@click.group()
@click.option('--debug', is_flag=True)
@click.pass_context
def cli(ctx, debug):

    ctx.ensure_object(dict)
    ctx.obj['debug'] = debug

@cli.command()
@click.argument('agent_name')
@click.option('-h', '--host')
@click.option('-p', '--port')
@click.option('-d', '--daemonize', is_flag=True)
@click.option('-c', '--config')
@click.option('-v', '--verbose', count=True)
@click.pass_context
def agent(
    ctx,
    agent_name: str,
    host: str,
    port: int,
    daemonize: bool,
    config: str,
    verbose: int
):

    opts = {
        'verbosity': verbose,
        'host':      host,
        'port':      port,
    }

    agent = Agent(
        agent_name=agent_name,
        debug=ctx.obj['debug'],
        config_path_str=config,
        cli_options=opts,
    )

    agent.run(daemonize)

@cli.command()
@click.option('-h', '--host')
@click.option('-p', '--port')
@click.option('-c', '--config')
@click.option('-v', '--verbose', count=True)
@click.pass_context
def server(ctx, host: str, port: int, config: str, verbose: int):

    opts = {
        'verbosity': verbose,
        'host':      host,
        'port':      port,
    }

    s = Server(
        config_path=config,
        cli_options=opts,
        debug=ctx.obj['debug']
    )

    s.run()

@cli.command()
@click.pass_context
def version(ctx):
    version = pkg_resources.require('cnc')[0].version
    print(f"Version: {version}")
