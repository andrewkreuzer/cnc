from __future__ import annotations

from pathlib import Path
import site
import logging

from cnc.errors import (
    CnCConfigNotFoundError,
)

class BaseConfig():

    def __init__(self) -> None:
        pass

    @staticmethod
    def get_path(config_path_str) -> Path:
        if (
            config_path_str
            and Path(config_path_str).is_file()
        ):
            config_path = Path(config_path_str)

        elif Path(site.USER_BASE + '/cnc/config.yml').is_file():
            config_path = Path(site.USER_BASE + '/cnc/config.yml')

        elif Path('/usr/local/cnc/config.yml').is_file():
            config_path = Path('/usr/local/cnc/config.yml')

        else:
            raise CnCConfigNotFoundError

        logging.info(f"Loading config from {config_path}")
        return config_path


