import asyncio
import logging
import sys
from collections.abc import Coroutine
from typing import Dict, List
import sys

from .message_broker import MessageBroker
from cnc.messaging import Message
from cnc.task import Task, State as TaskState
from cnc.errors import (
    CnCMessageBrokerNotFoundError,
)

class Scheduler():
    state = None
    message_broker = None

    def __init__(self, config):
        self.tasks: List[Task] = []
        self.outside_agent_tasks: Dict[str, List[Task]] = {}
        self.agent_config = config

    async def send_tick(self):
        if self.message_broker:
            await self.message_broker.send({
                'tasks': self.tasks,
            })

    def set_message_broker(self, message_broker: MessageBroker):
        self.message_broker = message_broker

    def get_message_broker(self) -> MessageBroker:
        if self.message_broker:
            return self.message_broker

        raise CnCMessageBrokerNotFoundError

    def run_state(self, update: str):
        self.state = update

    async def _run(self, runners: List[Coroutine]):
        for tcoro in asyncio.as_completed(runners):
            ct = await tcoro

            await self.send_tick()

    async def run(self):

        def check_dependencies(task: Task) -> bool:
            for dep in task.depends_on:
                for t in self.tasks:
                    if (
                        dep.task_name == t.name
                        and not t.state == TaskState.COMPLETED
                    ):
                        return False

                if dep.agent:
                    if dep.task_name not in [
                        t.name for t in
                        self.outside_agent_tasks.get(dep.agent, [])
                    ]:
                        return False

            return True

        while True:

            runners = []
            for t in self.tasks:
                if (
                    t.state == TaskState.DEPENDANT
                    and check_dependencies(t)
                ):
                    t.state = TaskState.WAITING

                if t.state == TaskState.WAITING:
                    runners.append(t.run(self.agent_config.verbosity))

            await self._run(runners)
            await self.send_tick()
            await asyncio.sleep(0.5)

    def update_from_message(self, message: Message):
        if message.error:
            logging.error(message.error)
            sys.exit(1)

        for aname, tasks in message.tasks.items():
            if aname != self.message_broker.agent_name:
                logging.info(
                    f"Adding tasks from agent: {aname} "
                    f"-> {[t.name for t in tasks]}"
                )
                try:
                    self.outside_agent_tasks[aname].extend(tasks)
                except:
                    self.outside_agent_tasks[aname] = tasks
            else:
                # TODO: cancelable tasks
                for mt in tasks:
                    if mt not in self.tasks:
                        self.tasks.append(mt)

