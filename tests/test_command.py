from cnc.command import Command

class TestCommand():
    test_command = Command(cmd='ls', cwd='.')
    test_command_json = (
        '{'
            '"cmd": "ls", '
            '"cwd": ".", '
            '"successful": false'
        '}'
    )

    def test_default_init(self):
        assert self.test_command.cmd == 'ls'
        assert self.test_command.cwd == '.'

    def test_from_dict(self):
        dict_command = {
            'cmd': 'ls',
            'cwd': '.',
        }

        from_dict_command = Command.from_dict(dict_command)

        self.test_command == from_dict_command

    def test_from_json(self):

        assert Command.from_json(self.test_command_json) == self.test_command

    def test_to_json(self):

        assert self.test_command.to_json() == self.test_command_json



